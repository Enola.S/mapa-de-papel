
var carte = document.getElementById('mapid');
var recup = document.getElementById('recup');
var formulaireCode = document.getElementById('formCode');
var table = document.getElementById("images");

var startTime = 0
var start = 0
var end = 0
var diff = 0
var timerID = 0

function chrono(){
  end = new Date();
  diff = end - start;
  diff = new Date(diff);
  var msec = diff.getMilliseconds();
  var sec = diff.getSeconds();
  var min = diff.getMinutes();
  if (min==12){
    var data4 = 'tps=' + "12:00:00";
    fetch('MaJJoueur.php', {
      method: 'post',
      body: data4,
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    })
    .then(r => r.text())
    .then(r=>{
    //on arrete le temps
      clearTimeout(timerID);
      alert('Vous avez échoué.. Retentez votre chance.');
      document.location.href='index.html';
    })
  }
  if (min < 10){
    min = "0" + min;
  }
  if (sec < 10){
    sec = "0" + sec;
  }
  if(msec < 10){
    msec = "00" +msec;
  }
   else if(msec < 100){
     msec = "0" +msec;
   }
  document.getElementById("chronotime").innerHTML = min + ":" + sec + ":" + msec;
  timerID = setTimeout("chrono()", 10);
}
function chronoStart(){
  start = new Date();
  chrono();
}

function chronoStop(){
  clearTimeout(timerID);
}


fetch('papel.php', {
  method: 'post',
})
.then(r => r.json())
 .then(r=>{
  formulaireCode.style.visibility = "hidden";
  table.style.visibility = "hidden";
  chronoStart();
  chargeDebut(r);
  objetsCarte=[1];
 }
)


function chargeDebut(objet){
    mymap = L.map('mapid').setView([objet.latitude, objet.longitude], 28);
  L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
  }).addTo(mymap);

  chargementMarqueur(objet);
}




function ajoutObjet(IdObj){

  var data = 'id='+IdObj;
  fetch('ajoutObjet.php', {
  method: 'post',
  body: data,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }

})
.then(r => r.json())
.then(r=>{
  chargementMarqueur(r);
  objetsCarte.push(IdObj);
 })
}

function generationObjet(obj){
  ajoutObjet(obj.genere);
  //on genere l objet qu'il genere
  obj.genere=0;
}

function recuperationObjet(obj,marqueur){
  var emplacement = "case" + obj.id;
  var recup = document.getElementById(emplacement);
  recup.style.visibility='visible';
  //on le met dans les objets recuperes
  mymap.removeLayer(marqueur);
  var indice = objetsCarte.indexOf(obj.id);
  //on enleve le marqueur de la carte
  obj.recuperable=0;
}




 function chargementMarqueur(obj){
  var BraqueurIcon = L.Icon.extend({
    options: {
      iconSize:     [95, 95], // size of the icon
      iconAnchor:   [22, 94], // point of the icon which will correspond to marker's location
      popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
    }
  });

    var marqueur = L.marker([obj.latitude, obj.longitude],{icon: new BraqueurIcon({iconUrl: "images/" +obj.id_icone })}).bindPopup(obj.indice);
    mymap.on('zoom', function() {
      var currentZoom = mymap.getZoom();
      if (currentZoom < obj.zoom) {
        marqueur.setOpacity(0);
      }
      else{
        marqueur.setOpacity(1);
      }
    })
    //si l objet est le code de Lisbonne, on ne l affiche pas
    marqueur.on('click', function(e){
      if (obj.nom=="knox"){
        //FIN DU GAME
          var temps = document.getElementById('chronotime').innerHTML.substring(0,8);
          var data3 = 'tps='+ temps;
          fetch('MaJJoueur.php', {
            method: 'post',
            body: data3,
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded'
            }
          })
          .then(r => r.text())
          .then(r=>{
            //on recupere le temps
            //on arrete le temps
            alert('Bravo, vous avez réussi à merveille votre mission avec un temps de : ' + temps);
            clearTimeout(timerID);
            document.location.href='fin2.html';
           })
      }
      marqueur.openPopup();
      if (obj.recuperable==1){
        if(obj.bloque_par>0){//si l objet est bloque
          var data2= 'id='+obj.bloque_par;
          fetch('ajoutObjet.php', {
          method: 'post',
          body: data2,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        })
        .then(s => s.json())
        .then(s=>{
          //objet bloquant du marqueur
          if (objetsCarte.includes(obj.bloque_par)==false){
            ajoutObjet(obj.bloque_par);
          }
          if (s.type=="objet"){
            // objet bloque par un objet
            var c = "case" + obj.bloque_par;
            var choix = document.getElementById(c);
            if(choix.style.visibility!="hidden"){
              //on regarde si l objet bloquant est dans les objets recuperes
              var id = "id" + obj.bloque_par;
              var radio = document.getElementById(id);
              if(radio.checked==true){
                //on regarde si l objet bloquant est selectionne
                obj.bloque_par=0;
                //on debloque l objet
                recuperationObjet(obj,this);
                //on le recupere
                if (obj.genere>0){
                  generationObjet(obj);
                }
             }
           }
         }
         else{
           // objet bloque par un code
           formulaireCode.style.visibility='visible';
           //on affiche le formulaire pour ecrire le code
           formulaireCode.addEventListener('submit', function(e1){
             e1.preventDefault();
             var champ = document.getElementById('champ');
             if (champ.value==s.nom){
               obj.bloque_par=0;
               //on debloque l objet
               recuperationObjet(obj,marqueur);
               //on le recupere
               if (obj.genere>0){
                 generationObjet(obj);
               }
               formulaireCode.style.visibility='hidden';
               //on retire le formulaire
               champ.value="";
             }

           })
         }
       })
     }
      else{
        marqueur.on('click', function(e3){
        //si l objet n est pas bloque
          recuperationObjet(obj,this);
          //on le recupere
          if (obj.genere>0){
            generationObjet(obj);
          }
        })
      }
    }
      else{
        marqueur.on('click', function(e2){
          //si l objet n est pas bloque
          //objet non recuperable
           if (obj.genere>0){
             generationObjet(obj);
           }
           mymap.removeLayer(marqueur);
        })
      }
    })
    marqueur.addTo(mymap);
}
