var banniere = document.getElementById('banniere');
var texte = banniere.firstElementChild;
var tailleTexte = banniere.scrollWidth;


function meilleurTemps(r){
	var minMinute = parseFloat(r[0].temps.substring(0,2));
  var minSec = parseFloat(r[0].temps.substring(3,5));
	var minMilli = parseFloat( r[0].temps.substring(6,9));
  var iMin = 0;
	for(let i=0; i<r.length; i++){
		var tpsMinute = parseFloat(r[i].temps.substring(0,2));
    var tpsSec =parseFloat( r[i].temps.substring(3,5));
    var tpsMilli =  parseFloat(r[i].temps.substring(6,9));
    if (tpsMinute <= minMinute){
      if (tpsSec <= minSec){
        if (tpsMilli <= minMilli){
          minMilli= tpsMilli;
          minSec = tpsSec;
          minMinute = tpsMinute;
          iMin = i;
        }
      }
    }
	}
	return iMin;
}


fetch('recuperationTab.php', {
  method: 'post',
})
.then(r => r.json())
 .then(r=>{
  for(let i=0; i<10; i++){
  	  texte.innerHTML+= 'Bravo! Tu peux maintenant regarder si ton score est présent dans la liste des trois meilleurs joueurs. &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 &#160 ';
  }
  defile();

  document.getElementById('nomPremier').innerHTML = r[0].nom;
  document.getElementById('scorePremier').innerHTML = r[0].temps;

  document.getElementById('nomDeuxieme').innerHTML = r[1].nom;
  document.getElementById('scoreDeuxieme').innerHTML = r[1].temps;

  document.getElementById('nomTroisieme').innerHTML = r[2].nom;
  document.getElementById('scoreTroisieme').innerHTML = r[2].temps;


 })


function defile(){
     var pos = texte.style.marginLeft.replace('px','');
     pos -= 10;
     texte.style.marginLeft = pos+"px";
     setTimeout(defile, 100);
     if(pos < -tailleTexte){
          pos = 200;
     }

}
