-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Dec 06, 2019 at 06:34 PM
-- Server version: 5.7.26
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `game`
--

-- --------------------------------------------------------

--
-- Table structure for table `joueurs`
--

CREATE TABLE `joueurs` (
  `id` bigint(20) NOT NULL,
  `nom` text NOT NULL,
  `temps` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `joueurs`
--

INSERT INTO `joueurs` (`id`, `nom`, `temps`) VALUES
(1, 'Martina', '06:02:56'),
(2, 'Jeanne', '03:12:00'),
(3, 'Popol', '10:23:12'),
(4, 'Jbdu77', '08:45:02'),
(5, 'franki', '11:54:48');

-- --------------------------------------------------------

--
-- Table structure for table `objets`
--

CREATE TABLE `objets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nom` text NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `zoom` int(11) NOT NULL,
  `id_icone` text NOT NULL,
  `type` text NOT NULL,
  `recuperable` tinyint(1) DEFAULT NULL,
  `bloque_par` int(11) NOT NULL,
  `genere` int(11) NOT NULL,
  `indice` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `objets`
--

INSERT INTO `objets` (`id`, `nom`, `latitude`, `longitude`, `zoom`, `id_icone`, `type`, `recuperable`, `bloque_par`, `genere`, `indice`) VALUES
(1, 'prof_debut', 40.422753, -3.67084, 13, 'professeur.png', 'objet', 0, 0, 2, 'J\'ai besoin de toi pour reformer mon equipe. Après des mois de recherche, j\'ai enfin mis au point un nouveau braquage. Aide moi à retrouver mes associés. La dernière que j\'ai vue est celle qui m\'est la plus chère. Clique pour continuer.'),
(2, 'lisbonne', 38.723287, -9.139323, 11, 'lisbonne.png', 'objet', 1, 23, 3, 'Qui t\'envoies? Si je peux te faire confiance tu partiras trouver 3 indices que j\'ai disposés dans la ville qui t\'aideront à trouver l\'une des nôtres.'),
(3, 'nez', 38.7238, -9.1245, 10, 'nez.png', 'objet', 0, 0, 4, 'Clique pour trouver la suite du rébus.'),
(4, 'robe', 38.7088, -9.1245, 10, 'robe.png', 'objet', 0, 0, 5, 'Clique pour trouver la suite du rébus.'),
(5, 'i', 38.7148, -9.1423, 10, 'i.png', 'objet', 0, 0, 6, 'Rends-toi à la prochaine étape. Clique pour continuer.'),
(6, 'nairobi', -1.283253, 36.817245, 8, 'nairobi.png', 'objet', 1, 7, 10, 'Moi je ne pars pas sans mon binôme.'),
(7, 'helsinki', 60.1674098, 24.9425769, 8, 'helsinki.png', 'objet', 1, 8, 0, 'Moi je ne partirai pas sans mon acolyte du nord.'),
(8, 'oslo', 59.9133301, 10.7389701, 8, 'oslo.png', 'objet', 1, 0, 9, 'Oslo est sur la défensive et pointe son arme sur toi. Clique pour le ramener à la raison.'),
(9, 'arme_lourde', 59.9133301, 10.7389701, 13, 'arme_lourde.png', 'objet', 1, 0, 0, 'Clique pour récupérer l\'arme..elle pourrait servir.'),
(10, 'telephone', -1.283253, 36.817245, 13, 'telephone.png', 'objet', 0, 0, 11, 'El professor : Maintenant que tu as trouvé la moitié du groupe, tu dois te protéger en récupérant une autre arme avant d\'aller chercher un des braqueurs sur le même continent. Suis l\'équateur et rends toi sur un continent dangereux... dans une capitale connue. Clique pour continuer.'),
(11, 'arme_quito', -0.2201641, -78.5123274, 7, 'arme_quito.png', 'objet', 1, 0, 12, 'Clique pour récupérer l\'arme. Attention: n\'oublie pas de récupérer un des braqueurs dans une capitale dangereuse de ce continent'),
(12, 'rio', -22.9110137, -43.2093727, 10, 'rio.png', 'objet', 1, 13, 15, 'J\'ai besoin de ma moitié mais elle ne viendra pas si tu ne lui transmets pas un code.'),
(13, 'tokyo', 35.6828387, 139.7594549, 6, 'tokyo.png', 'objet', 1, 14, 0, 'L\'espoir c\'est comme les ___ . Une fois que l\'un tombe, les autres suivent. [si tu ne trouves pas le code, rends toi aux pieds d\'une des 7 merveilles du monde]'),
(14, 'dominos', -22.9493, -43.1563, 13, 'dominos.png', 'code', 0, 0, 0, 'Maintenant que tu m\'as trouvé, tu peux retourner à Tokyo.'),
(15, 'indice_rio', -22.9110137, -43.2093727, 13, 'indice_rio.png', 'code', 0, 0, 16, 'Retrouve père et fils. Clique pour continuer.'),
(16, 'denver', 39.7392364, -104.9848623, 8, 'denver.png', 'objet', 1, 0, 17, 'J\'ai besoin d\'une arme pour libérer mon père qui est ici avec moi. Il est détenu en milieu sauvage. Clique pour continuer.'),
(17, 'moscou', 39.7492422, -104.9504003, 8, 'moscou.png', 'objet', 1, 9, 18, 'Aide moi je n\'ai pas d\'arme... '),
(18, 'indice_moscou', 39.7492422, -104.9504003, 10, 'indice_moscou.png', 'code', 0, 0, 19, 'Afin de compléter la mission du professeur, essayons d\'obtenir de l\'aide auprès d\'un membre important de l\'équipe, quitte à se heurter à un mur. Clique pour continuer.'),
(19, 'berlin', 52.5170365, 13.3888599, 8, 'berlin.png', 'objet', 1, 0, 20, 'Clique et retourne au QG.'),
(20, 'prof_fin', 40.422753, -3.67084, 13, 'professeur.png', 'objet', 0, 0, 21, 'Bravo nous sommes au complet grâce à toi. J\'espère que tu es armé, récupères les plans du prochain braquage dans la casa de Papel. Clique pour continuer.'),
(21, 'plan_braquage', 40.422753, -3.67084, 10, 'plan_braquage.png', 'objet', 1, 11, 24, 'Attention! Utilise une arme par encore utilisée pour avoir accès au plan.'),
(22, 'knox', 37.883369, -85.965304, 6, 'knox.png', 'objet', 0, 0, 0, ''),
(23, 'el professor', 0, 0, 25, 'code.png', 'code', 0, 0, 0, ''),
(24, 'indice_braquage', 40.422753, -3.67084, 13, 'indices_braquage.png', 'objet', 0, 0, 22, 'Rends toi sur le lieu du braquage. Clique pour continuer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `joueurs`
--
ALTER TABLE `joueurs`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `joueurs`
--
ALTER TABLE `joueurs`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=139;
