# Escape Game Casa de Papel
L'Escape Game Casa de Papel est un jeu géographique visant à retrouver dans l'ordre des éléments dans un temps imparti.
## Pour commencer 
### Prérequis
Pour utilser Escape Game Casa de Papel il faut utiliser
- un navigateur.
- un webserver type [MAMP](https://www.mamp.info/fr/).
- le logiciel [phpMyAdmin](http://localhost:8888/phpMyAdmin/?lang=en).
### Installation
Pour installer le jeu,

1.  Depuis le dépot [GitlLab](https://gitlab.com/Enola.S/mapa-de-papel), copier le lien HTTPS comme suit:

![capture GitLab](https://docs.gitlab.com/ee/gitlab-basics/img/project_clone_url.png)

2. Ouvrir un **terminal** dans le repertoire htdocs de l'application MAMP et taper la commande suivante :
```
>>> git clone [lien https]
```

3. Ouvrir **MAMP** et allumer les serveurs. Vérifier que Apache Server et MySQL server sont cochés en vert
![capture Mamp](https://premium.wpmudev.org/blog/wp-content/uploads/2015/09/mamp-servers-working1.png)

Pour importer la base de donnée,

1. Depuis le logiciel **phpMyAdmin**, créer une base de données appellée *game*.

![capture phpMA](https://gitlab.com/Enola.S/mapa-de-papel/raw/master/images/newbase.tiff)

2. Dans l'onglet *import*, choisir le fichier *BDD.sql* du dépot Git.
![capture phpMA2](https://gitlab.com/Enola.S/mapa-de-papel/raw/master/images/import.png)
Garder tous les réglages par défaut et cliquer *Go*.
## Utilisation
### Démarrage

Pour lancer le jeu, ouvrir un navigateur et saisir *localhost* dans la barre url. La page de démarrage apparaît.
### Règles du jeu

>  Dans le dossier Annexes du dépot Git
>*  Le pdf *fiches_perso* vise à aider un joueur qui n'aurait jamais vu la Casa de Papel ou ne s'en souviendrait pas bien.
>*  Le pdf *reponses* révèle toutes les étapes pour finir le jeu.


Pour jouer il faut rentrer un pseudonyme qui identifiera la partie et non le joueur. Au clic sur Démarrer, un chronomètre est lancé. 

Pour commencer, cliquer sur le premier personnage. 

Le jeu présente des énigmes et des indices pour accéder à l'objet suivant.

> **Attention** Il faut parfois cliquer une fois de plus après avoir ouvert la bulle pop-up d'un personnage. Cela sera toujours précisé.
### But du jeu 
Le but est de finir la partie le plus rapidement possible. Au delà de **12:00:00 min**, la partie est automatiquement perdue et il est proposé de recommencer. 
Il faut retrouver tous les braqueurs et le lieu du prochain braquage de l'équipe de la série La Casa De Papel.
### Objets bloqués
Certains braqueurs ne peuvent être récupérés que si l'on séléctionne dans les Objets Récupérés le braqueur ou l'objet qui le libère.
### Objets récupérés
Dans la page de jeu, se placeront sous la carte les objets et personnages récupérés au cours de la partie. Un objet récupéré peut en débloquer un autre. 
> Si A est sur la carte et bloqué par B, si B est dans les objets récupérés, cliquer sur B puis cliquer sur A, A apparaît dans les objets récupérés.
### Fin du jeu
Le jeu s'arrête lorsque vous avez reconstitué la totalité de l'équipe et que vous récupérez le butin sur le lieu du nouveau braquage. Le score est indiqué et la page de fin s'affiche.
Si le temps atteint 12:00:00 min, le jeu s'arrête, c'est perdu. Le jeu revient sur la page d'accueil.
### Wall of fame
Sur la page de fin, un tableau récapitule les trois meilleures parties avec le pseudonyme du joueur et le score réalisé. 


## Auteurs
Sobreira Oriane
Sengeissen Enola

Décembre 2019


